/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });


$(document).ready(function() {
    /***** Para productos ********************************/
        $("#fr-producto").on("click","#guardar",function(e){
                e.preventDefault();
    
        var control = true;
        var arr = $(".validate");
    
            if (arr.length > 0) {
                for (i = 0; i < arr.length; i++) {
                if (arr[i].value === '' || arr[i].value === null || arr[i].value === " ") {
                    //alert("El campo '"+  arr[i].id + "' no puede estar vacio"); //Si quieres que imprima en específico que campo no puede dejar vacío
                    
                    $("#"+arr[i].id).css("border", "red solid 1px"); 
                    $("#"+arr[i].id).siblings('span').remove(); 
                    $("#"+arr[i].id).after("<span class='text-danger'>Este campo es obligatorio</span>"); 
    
                    control = false;
                    // break;
                }
                }
            }
    
        if (control) {
            var datosRegistro = $("#fr-producto").serialize();
            var formUrl = $("#fr-producto").attr("action");
            var rows = "";
    
            $.ajax({
            type: "POST",
            url:  formUrl,
            data: datosRegistro,
            beforeSend: function(){
            },
            success: function(data) {
    
                if(data.status == "200"){
    
                //alert('Registro guardado');
                //$("#fr-producto")[0].reset();
                swal("Correcto!",'Registro Guardado', "success");
                   location.reload();
                }
    
                
    
            }//fin del success
            });
        }else{
    
            alert("Faltan campos por llenar"); //o el mensaje para todos
        }
    
        });
    });