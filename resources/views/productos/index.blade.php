@extends('layouts.app')
@section('content')

        <!--MDB Inputs-->
        <div class="container mt-4">

          <!-- Grid row -->
           <div class="row">
            
                <!-- Grid column -->
                <div class="col-md-12">
            
                    <div class="card">
                      <div class="card-body">

                        <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong> Nuevo Producto </strong></h5>
                        <hr>

                         <form id="fr-producto" method="POST" action="{{url('productos/')}}">
                            <!-- Grid row -->
                             @csrf
                            
                            <input name="idProd" type="hidden" id="id">
                            
                            <div class="row mt-2 mt-2">
                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Nombre Producto</label>
                                    <input name="nombre_producto" type="text" id="nombres" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Referencia</label>
                                    <input name="referencia" type="text" id="referencia" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                            </div>
                            <!-- Grid row mt-2 -->
                             
                            <!-- Grid row mt-2 -->
                            <div class="row mt-2 mt-2">
                              <!-- Grid column -->
                                <div class="col-md-6" style="">
                                  <div class="md-form">
                                    <label for="form2" class="">Precio</label>
                                    <input name="precio" type="text" id="precio" class="form-control">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6" style="">
                                  <div class="md-form">
                                    <label for="form2" class="">Peso</label>
                                    <input name="peso" type="text" id="peso" class="form-control validate">
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Categoria</label>
                                    <select name="categoria" class="form-control validate" id="categoria">
                                      <option value=""> </option>
                                      <option value="Bebidas">Bebidas </option>
                                      <option value="Utencilios de cocina">Utencilios de cocina</option>
                                    </select>
                                  </div>
                            
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">
                                  <div class="md-form">
                                    <label for="form2" class="">Stock</label>
                                    <input name="stock" type="text" id="stock" class="form-control">
                                  </div>
                                </div>
                                <!-- Grid column -->
                            
                            </div>
                            <!-- Grid row -->
      
                            <div class="row mt-2 mt-2">

                              
                            
                            </div>
                            <!-- Grid row -->

                            <div class="row mt-2 mt-2">
                            
                            </div>
                            <!-- Grid row -->

                            <div class="row mt-2">
                              <!-- Grid column -->
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                            <div class="row mt-2 mr-2 float-right"> 
                             <p class="">
                              <button id="guardar" class="btn btn-secondary">  Guardar </button>
                             <!-- botones para editar un registro o cancelarlo -->
                              <button id="e-guardar" class="btn btn-info" style="display: none;">  Actualizar </button>
                              <button id="cancelar" class="btn btn-danger" style="display: none;"> Cancelar </button>
                             </p>
                            </div>
                          </form>
                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="row mt-3">
            
              <!-- Grid column -->
              <div class="col-md-12">
            
                  <div class="card">
                    <div class="card-body">
                     <h5 class="pt-3 pb-2 font-up col-md-push-6"><strong>Estudiantes </strong></h5>
                  <div class="table-responsive">

                
              <table id="TablaProductos" class="table table-bordred table-striped">
                   
                  <thead class="table-info"> 
                   <th><input name="" type="checkbox" id="checkall" /></th>
                    <th>Nombre Producto</th>
                    <th>Referencia</th>
                    <th>Precio</th>
                    <th>Peso</th>
                    <th>Categoria</th>
                    <th>Stock</th>
                    <th>Editar</th>  
                    <th>Borrar</th>
                  </thead>
                   
                   <tbody>

                   @foreach($productos as $value)
                    <tr class="Col{{$value->id}}">
                     <td><input type="checkbox" class="checkthis" /></td>
                     <td>{{$value->nombre_producto}}</td>
                     <td>{{$value->referencia}}</td>
                     <td>{{$value->precio}}</td>
                     <td>{{(!empty($value->peso))?$value->peso:''}}</td>
                     <td>{{(!empty($value->categoria))?$value->categoria:''}}</td>
                     <td>{{(!empty($value->stock))?$value->stock:''}}</td>
                     <td><a class="btn btn-primary btn-xs" id="editar-e" data-url="{{url('productos/'.$value->id.'/edit')}}" data-title="Editar" >Editar</a></td>
                     <td class="Delete{{$value->id}}"><a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-e" data-url="{{url('productos/'.$value->id)}}" data-id="{{$value->id}}">Borrar</a></td>
                    </tr>
                    @endforeach
                    
                    <!-- <tr class="Col">
                     <td><input type="checkbox" class="checkthis" /></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td><a class="btn btn-primary btn-xs" id="editar-e" data-title="Editar" >Editar</a></td>
                     <td class="Delete"><a class="btn btn-danger btn-xs" data-title="Delete" id="borrar-e" data-url="" data-id="">Borrar</a></td>
                    </tr> -->
     

                   </tbody>
                </table>
                  

               </div>    

                        </div>
                    </div>
            
                </div>
                <!-- Grid column -->
            </div>
        </div>
    <!--MDB Inputs-->
 @include('modales.modal_borrar')
<script>

</script>
@endsection