@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @role('Administrador')
            <div class="card" id="pie_chart" style="width:750px; height:400px";>
                <div class="card-header"></div>  
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>

                <!--Div that will hold the pie chart-->
                <div  align="center"></div>

            </div>
         
         @else

          <div class="card" id="" style="width:750px; height:400px";>
                <div class="card-header">{{ __('Estudiante') }}</div>  
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    <p><b>Nombre :</b> {{ Auth::user()->name }}</p>
                    <p><b>Email :</b>  {{ Auth::user()->email }}</p>
                    <p><b>Telefono :</b>  {{ (!empty(Auth::user()->StudentData()->phone))?Auth::user()->StudentData()->phone:'' }}</p>
                    <p><b>Programas :</b>  {{ Auth::user()->name }}</p>
                    </div>

                <!--Div that will hold the pie chart-->
                <div  align="center"></div>

            </div>

        @endrole
        </div>
    </div>
</div>
@role('Administrador')
<script type="text/javascript">
   var analytics = <?php echo $projects ?? ''; ?>

   google.charts.load('current', {'packages':['corechart']});

   google.charts.setOnLoadCallback(drawChart);

   function drawChart()
   {

    var data = google.visualization.arrayToDataTable(analytics);

    var options = {
     title : 'Usuarios Activos',
     //is3D: true,
    bar: {groupWidth: "95%"},
        legend: { position: "none" },
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('pie_chart'));
    chart.draw(data, options);
   
   }
</script>
@endrole
@endsection