<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre_producto' => ['required', 'string'],
            'referencia' => ['required', 'string'],
            'precio' => ['required'],
            'peso' => ['required'],
            'categoria' => ['required'],
            'stock' => ['required'],
        ];
    }
}
