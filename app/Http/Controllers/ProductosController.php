<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       // dd($request);
       // $product = Product::create($request);

        $product = new Product;
    	$product->nombre_producto = $request['nombre_producto'];
        $product->referencia = $request['referencia'];
        $product->precio = $request['precio'];
        $product->peso = $request['peso'];
        $product->categoria = $request['categoria'];
        $product->stock = $request['stock'];
    	$product->save();
        //

        return response()->json(['status' => 200,'data' => $product]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $productos)
    {
        //
        return response()->json(['status' => 200,'data' => $productos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $productos)
    {
      //
            $productos->nombre_producto = $request['nombre_producto'];
            $productos->referencia = $request['referencia'];
            $productos->precio = $request['precio'];
            $productos->peso = $request['peso'];
            $productos->categoria = $request['categoria'];
            $productos->stock = $request['stock'];
            $productos->update();

      return response()->json(['status' => 200,'data' => $productos]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::find($id)->delete();
      
      return response()->json(['status'=>200]);
    }
}
