<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@productos');
Route::get('productos/index', 'HomeController@productos');
Route::post('productos/', 'ProductosController@store');
//Route::get('/proyectos/index', 'ProjectsController@index');
Route::get('/productos/{productos}/edit', 'ProductosController@edit');
Route::put('/productos/{productos}', 'ProductosController@update');
Route::delete('/productos/{productos}', 'ProductosController@destroy');

Route::get('ventas/index', 'VentasController@index');
Route::post('ventas/', 'VentasController@store');
//Route::get('/proyectos/index', 'ProjectsController@index');
//Route::get('/ventas/{venta}/edit', 'ProductosController@edit');
//Route::put('/productos/{productos}', 'ProjectsController@update');
//Route::delete('/productos/{productos}', 'ProjectsController@destroy');
